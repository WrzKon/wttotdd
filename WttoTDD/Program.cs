﻿using Bank;
using Bank.DB;
using Bank.Utils;
using System;

namespace WttoTDD
{
    class Program
    {
        static string Menu => @"Zeby wybrac operacji wcisnij przycisk:
1. Wplac gotowke
2. Wyplac gotowke
3. Wyslij przelew
4. Sprawdz historie transakcji
5. Wyloguj";
        static string AdminMenu => Menu + Environment.NewLine + "6. Zobacz salda wszystkich uzytkownikow";

        static void Main(string[] args)
        {
            IAccount account;
            bool isAdmin;
            while (!LogIn(out account, out isAdmin))
            {
                Console.WriteLine("Bledne dane logowania");
                Console.ReadLine();
                Console.Clear();
            }

            AccountOperationsLoop(account, isAdmin);
        }

        private static bool LogIn(out IAccount account, out bool isAdmin)
        {
            Console.WriteLine("Podaj login");
            var login = Console.ReadLine();
            Console.WriteLine("Podaj haslo");
            var password = Console.ReadLine();

            var bank = new Bank.Domain.Bank(new BankDB(), new FileManager());
            return bank.LogIn(login, password, out account, out isAdmin);
        }

        static void AccountOperationsLoop(IAccount account, bool isAdmin)
        {
            int operation = 0;
            while (true)
            {
                Console.Clear();
                Console.WriteLine($"Twoje saldo: {account.Balance}");
                if(isAdmin)
                    Console.WriteLine(AdminMenu);
                else
                    Console.WriteLine(Menu);
                operation = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                switch (operation)
                {                    
                    case 1:
                        if (!account.Deposit())
                        {
                            Console.WriteLine("Bledny format czekow.");
                            Console.ReadLine();
                        }
                        break;

                    case 2:
                        Console.WriteLine("Jaka kwote chcesz wyplacic?");
                        var moneyToWithdraw = Convert.ToDecimal(Console.ReadLine());
                        if (!account.Withdraw(moneyToWithdraw))
                        {
                            Console.WriteLine("Kwota przekroczyła saldo konta.");
                            Console.ReadLine();
                        }
                        break;

                    case 3:
                        Console.WriteLine("Jaka kwote chcesz przeslac?");
                        var moneyToTransfer = Convert.ToDecimal(Console.ReadLine());
                        Console.WriteLine("Na jaki numer konta?");
                        var accountNumber = Console.ReadLine();
                        if (!account.Transfer(accountNumber, moneyToTransfer))
                        {
                            Console.WriteLine("Bledne dane przelewu.");
                            Console.ReadLine();
                        }
                        break;

                    case 4:
                        Console.WriteLine(account.ShowOperationHistory());
                        Console.ReadLine();
                        break;

                    case 5:
                        Console.WriteLine("Zostaniesz wylogowany");
                        Console.ReadLine();
                        return;

                    case 6:
                        if (!isAdmin)
                            Console.WriteLine("Niepoprawny wybor");
                        else
                            Console.WriteLine(account.GenerateAllUsersBalanceRaport());

                        Console.ReadLine();
                        break;

                    default:
                        Console.WriteLine("Niepoprawny wybor");
                        Console.ReadLine();
                        break;
                }
            }
        }
    }
}
