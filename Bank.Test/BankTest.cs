﻿using Bank.DB;
using Bank.Domain;
using Bank.Test.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Bank.Test
{
    [TestClass]
    public class BankTest
    {
        [TestMethod]
        public void LogIn_Correct_Test()
        {
            var bankDB = new TestDB();
            var fileManager = new TestFileManager();
            IBank bank = new Domain.Bank(bankDB, fileManager);

            var result = bank.LogIn("user1", "pass1", out IAccount a, out bool isAdmin);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LogIn_Incorrect_Test()
        {
            var bankDB = new TestDB();
            var fileManager = new TestFileManager();
            IBank bank = new Domain.Bank(bankDB, fileManager);

            var result = bank.LogIn("test", "test", out IAccount a, out bool isAdmin);
            Assert.IsFalse(result);
        }
    }
}
