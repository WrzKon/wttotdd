﻿using Bank.DB;
using Bank.Test.Utils;
using Bank.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Bank.Test
{
    [TestClass]
    public class AccountTest
    {

        [TestMethod]
        public void Deposit_Test()
        {
            const decimal verifyChequesMoney = 50;

            var bankDB = new TestDB();
            var fileManager = new TestFileManager()
                .WithVerifyChequesMoney(verifyChequesMoney);
            var account = new Account(bankDB, fileManager, bankDB.GetUser());

            var previousBalance = account.Balance;
            account.Deposit();

            Assert.AreEqual(previousBalance + verifyChequesMoney, account.Balance);
            bankDB.ClearTestDB();
        }

        [TestMethod]
        public void Withdraw_Test()
        {
            const decimal money = 30;

            var bankDB = new TestDB();
            var fileManager = new TestFileManager();
            var account = new Account(bankDB, fileManager, bankDB.GetUser());

            var previousBalance = account.Balance;
            account.Withdraw(money);

            Assert.AreEqual(previousBalance - money, account.Balance);
            bankDB.ClearTestDB();
        }

        [TestMethod]
        public void Transfer_Test()
        {
            const string accountNumber = "accNo2";
            const decimal money = 100;

            var bankDB = new TestDB();
            var fileManager = new TestFileManager();
            var account = new Account(bankDB, fileManager, bankDB.GetUser());

            var previousBalance = account.Balance;
            var previousTransferTargetBalance = bankDB.GetTransferTargetUser().Balance;
            account.Transfer(accountNumber, money);

            Assert.AreEqual(previousBalance - money, account.Balance);
            Assert.AreEqual(previousTransferTargetBalance + money, bankDB.GetTransferTargetUser().Balance);
            bankDB.ClearTestDB();
        }      

        [TestMethod]
        public void GenerateAllUsersBalanceRaport_Test()
        {
            const string raportMock = 
@"user1 - 200
user2 - 500
admin - 400
";

            var bankDB = new TestDB();
            var fileManager = new TestFileManager();
            var account = new Account(bankDB, fileManager, bankDB.GetUser());

            var raport = account.GenerateAllUsersBalanceRaport();

            Assert.AreEqual(raport, raportMock);
            bankDB.ClearTestDB();
        }        
    }
}
