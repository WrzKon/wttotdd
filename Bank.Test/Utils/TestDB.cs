﻿using Bank.DB;
using Bank.DB.Models;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Test.Utils
{
    public class TestDB : BankDB
    {
        const string testDBName = "MyTest";

        public TestDB() : base(testDBName) {}

        public User GetUser()
        {
            using (var db = new LiteDatabase(testDBName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName);
                return users.FindAll().First();
            }
        }

        public User GetTransferTargetUser()
        {
            using (var db = new LiteDatabase(testDBName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName);
                return users.FindAll().Skip(1).First();
            }
        }

        public void ClearTestDB()
        {
            if (File.Exists(testDBName))
                File.Delete(testDBName);
        }
    }
}
