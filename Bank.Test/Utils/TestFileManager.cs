﻿using Bank.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Test.Utils
{
    public class TestFileManager : IFileManager
    {
        private decimal _verifyChequesMoney;

        public TestFileManager WithVerifyChequesMoney(decimal moneyToVerifyCheques)
        {
            _verifyChequesMoney = moneyToVerifyCheques;
            return this;
        }

        public void GenerateCheque(decimal money)
        {
        }

        public decimal VerifyCheques()
        {
            return _verifyChequesMoney;
        }
    }
}
