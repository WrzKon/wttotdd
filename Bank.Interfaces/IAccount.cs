﻿using System.Collections.Generic;

namespace Bank.Intefaces
{
    public interface IAccount
    {
        string AccoutNumber { get; }
        decimal Balance { get; }
        IEnumerable<IOperationDetails> History { get; }
        void Deposit(decimal money);
        void Withdraw(decimal money);
        void Transfer(string targetAccountNumber, decimal money);
    }
}
