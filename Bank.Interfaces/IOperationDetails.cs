﻿using System;

namespace Bank.Intefaces
{
    public interface IOperationDetails
    {
        DateTime OperationDate { get; }
        decimal Amount { get; }
        OperationType OperationType { get; }
    }
}
