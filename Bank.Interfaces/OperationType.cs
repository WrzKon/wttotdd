﻿namespace Bank.Intefaces
{
    public enum OperationType
    {
        Withdrawal,
        Deposit,
        TransferIn,
        TransferOut
    }
}
