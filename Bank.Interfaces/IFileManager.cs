﻿namespace Bank.Interfaces
{
    public interface IFileManager
    {
        decimal VerifyCheque(string pathToCheque);
        void GenerateCheque(decimal money, string chequePath);
    }
}
