﻿using Bank.Interfaces;
using System;
using System.IO;

namespace Bank.DB
{
    public class FileManager : IFileManager
    {
        string ChequePattern { get
            {
                return "***({0})***";
            } }

        public void GenerateCheque(decimal money, string chequePath = null)
        {
            if (String.IsNullOrEmpty(chequePath))
                chequePath = Directory.GetCurrentDirectory();

            CreateFile(chequePath, String.Format(ChequePattern, money));
        }

        public decimal VerifyCheque(string chequePath)
        {
            return 0;
        }

        void CreateFile(string path, string content)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            File.WriteAllText(path, content);
        }
    }
}
