﻿namespace Bank.Domain
{
    public enum OperationType
    {
        Withdrawal,
        Deposit,
        TransferIn,
        TransferOut
    }
}
