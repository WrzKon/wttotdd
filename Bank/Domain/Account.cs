﻿using Bank.DB;
using Bank.DB.Models;
using Bank.Domain;
using Bank.Utils;
using System;
using System.Collections.Generic;

namespace Bank
{
    public interface IAccount
    {
        string AccoutNumber { get; }
        decimal Balance { get; }
        bool Deposit();
        bool Withdraw(decimal money);
        bool Transfer(string targetAccountNumber, decimal money);
        string ShowOperationHistory();
        string GenerateAllUsersBalanceRaport();
    }

    public class Account : IAccount
    {
        IFileManager _fileManager;
        IBankDB _bankDB;
        User _loggedUser;
        
        public string AccoutNumber => _loggedUser.AccountNumber;
        public decimal Balance => _loggedUser.Balance;


        public Account(IBankDB bankDB, IFileManager fileManager, User loggedUser)
        {
            _bankDB = bankDB;
            _fileManager = fileManager;
            _loggedUser = loggedUser;
        }

        public bool Deposit()
        {
            var money = _fileManager.VerifyCheques();
            if (money <= 0)
                return false;
            
            _bankDB.Deposit(_loggedUser, money);
            return true;
        }

        public bool Withdraw(decimal money)
        {
            money = Math.Round(money, 2);
            if (_loggedUser.Balance >= money)
            { 
                _fileManager.GenerateCheque(money);
                _bankDB.Withdraw(_loggedUser, money);
                return true;
            }
            else
                return false;
        }

        public bool Transfer(string targetAccountNumber, decimal money)
        {
            money = Math.Round(money, 2);
            if (_loggedUser.AccountNumber != targetAccountNumber && _loggedUser.Balance >= money)
            {
                return _bankDB.Transfer(_loggedUser, targetAccountNumber, money);
            }
            else
                return false;
        }

        public string ShowOperationHistory()
        {
            string operationHistory = string.Empty;

            foreach(var operation in _bankDB.GetOperationHistory(_loggedUser.Id))
            {
                operationHistory += $"Type: {operation.OperationType}, Date: {operation.OperationDate}, Amount: {operation.Amount}" + Environment.NewLine;
            }

            return operationHistory;
        }

        public string GenerateAllUsersBalanceRaport()
        {
            return _bankDB.GenerateAllUsersBalanceRaport();
        }
    }
}
