﻿using System;

namespace Bank.Domain
{
    public interface IOperationDetails
    {
        DateTime OperationDate { get; set; }
        decimal Amount { get; set; }
        OperationType OperationType { get; set; }
    }

    public class OperationDetails : IOperationDetails
    {
        public DateTime OperationDate { get; set; }
        public decimal Amount { get; set; }
        public OperationType OperationType { get; set; }

        public OperationDetails() {}

        public OperationDetails(decimal amount, DateTime operationDate, OperationType operationType) : base()
        {
            Amount = amount;
            OperationDate = operationDate;
            OperationType = operationType;
        }
    }
}
