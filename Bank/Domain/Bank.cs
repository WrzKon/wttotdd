﻿using Bank.DB;
using Bank.DB.Models;
using Bank.Utils;

namespace Bank.Domain
{
    public interface IBank
    {
        bool LogIn(string login, string password, out IAccount account, out bool isAdmin);
    }

    public class Bank : IBank
    {
        IBankDB _bankDB;
        IFileManager _fileManager;
        
        public Bank(IBankDB bankDB, IFileManager fileManager)
        {
            _bankDB = bankDB;
            _fileManager = fileManager;
        }

        public bool LogIn(string login, string password, out IAccount account, out bool isAdmin)
        {
            account = null;
            isAdmin = false;

            if (_bankDB.LogIn(login, password, out User loggedUser))
            {
                account = new Account(_bankDB, _fileManager, loggedUser);
                isAdmin = loggedUser.IsAdmin;
                return true;
            }

            return false;
        }
    }
}
