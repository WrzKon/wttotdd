﻿using System;
using System.IO;using System.Linq;
using System.Text;

namespace Bank.Utils
{
    public interface IFileManager
    {
        decimal VerifyCheques();
        void GenerateCheque(decimal money);
    }

    public class FileManager : IFileManager
    {
        public void GenerateCheque(decimal money)
        {
            if (!Directory.Exists(BankUtils.ChequesPath))
                Directory.CreateDirectory(BankUtils.ChequesPath);

            var path = $"{Path.Combine(BankUtils.ChequesPath, DateTime.Now.ToString("yyyyMMddHHmmss"))}.txt";

            if (File.Exists(path))
                File.Delete(path);

            using (var fs = File.CreateText(path))
            {
                fs.Write(money.ToString("0.##"));
            }
        }

        public decimal VerifyCheques()
        {
            if (!Directory.Exists(BankUtils.ChequesPath))
                return 0;

            decimal result = 0;

            var files = Directory.GetFiles(BankUtils.ChequesPath);

            foreach(var file in files)
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    var fileText = sr.ReadToEnd();
                    if (decimal.TryParse(fileText, out decimal parsedNumber))
                        result += parsedNumber;
                }
                File.Delete(file);
            }

            return Math.Round(result,2);
        }        
    }
}
