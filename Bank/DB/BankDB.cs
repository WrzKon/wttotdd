﻿using Bank.DB.Models;
using Bank.Domain;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace Bank.DB
{
    public interface IBankDB
    {
        bool LogIn(string login, string password, out User loggedUser);
        void Deposit(User user, decimal money);
        void Withdraw(User user, decimal money);
        bool Transfer(User user, string targetAccountNumber, decimal money);
        IEnumerable<OperationDetails> GetOperationHistory(int userId);
        string GenerateAllUsersBalanceRaport();
    }

    public class BankDB : IBankDB
    {
        readonly string _dbName;
        public BankDB(string dbName = "MyData.db")
        {
            _dbName = dbName;
            if (!File.Exists(_dbName))
                InitDB();
        }

        void InitDB()
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName);
                foreach (var user in BankDBUtils.InitUsers)
                {
                    users.Insert(user);
                }
            }
        }

        private User GetUser(Expression<Func<User, bool>> filter)
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName);
                return users.FindOne(filter);
            }
        }

        public bool LogIn(string login, string password, out User loggedUser)
        {
            loggedUser = GetUser(u => u.Login == login && u.Password == password);
            if (loggedUser == null)
                return false;

            return true;
        }

        private void UpdateUser(User user)
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName);
                users.Update(user);
            }
        }

        public void Deposit(User user, decimal money)
        {
            user.Balance += money;
            UpdateUser(user);
            ArchiveOperationDetails(
                new Operation()
                {
                    UserId = user.Id,
                    OperationDetails = new OperationDetails(money, DateTime.Now, OperationType.Deposit)
                });
        }

        public void Withdraw(User user, decimal money)
        {
            user.Balance -= money;
            UpdateUser(user);
            ArchiveOperationDetails(
                new Operation()
                {
                    UserId = user.Id,
                    OperationDetails = new OperationDetails(money, DateTime.Now, OperationType.Withdrawal)
                });
        }

        public bool Transfer (User user, string targetAccountNumber, decimal money)
        {
            var targetUser = GetUser(u => u.AccountNumber == targetAccountNumber);
            if (targetUser == null)
                return false;

            targetUser.Balance += money;
            UpdateUser(targetUser);
            ArchiveOperationDetails(
                new Operation()
                {
                    UserId = targetUser.Id,
                    OperationDetails = new OperationDetails(money, DateTime.Now, OperationType.TransferIn)
                });

            user.Balance -= money;
            UpdateUser(user);
            ArchiveOperationDetails(
                new Operation()
                {
                    UserId = user.Id,
                    OperationDetails = new OperationDetails(money, DateTime.Now, OperationType.TransferOut)
                });

            return true;
        }

        private void ArchiveOperationDetails(Operation operation)
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var operations = db.GetCollection<Operation>(BankDBUtils.OperationsTableName);
                operations.Insert(operation);
            }
        }

        public IEnumerable<OperationDetails> GetOperationHistory(int userId)
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var operations = db.GetCollection<Operation>(BankDBUtils.OperationsTableName);
                return operations.Find(o => o.UserId == userId).Select(o => o.OperationDetails);
            }
        }

        public string GenerateAllUsersBalanceRaport()
        {
            string raport = string.Empty;

            using (var db = new LiteDatabase(_dbName))
            {
                var users = db.GetCollection<User>(BankDBUtils.UsersTableName).FindAll();
                
                foreach(var user in users)
                {
                    raport += $"{user.Login} - {user.Balance}" + Environment.NewLine;
                }
            }

            return raport;
        }
    }
}
