﻿using Bank.Domain;

namespace Bank.DB.Models
{
    public class Operation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public OperationDetails OperationDetails { get; set; } 
    }
}
