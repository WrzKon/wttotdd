﻿using Bank.DB.Models;
using System.Collections.Generic;

namespace Bank.DB
{
    public static class BankDBUtils
    {
        public static string UsersTableName { get { return "users"; } }
        public static string OperationsTableName { get { return "operations"; } }
        public static List<User> InitUsers {
            get {
                return new List<User>()
                {
                    new User()
                    {
                        Login = "user1",
                        Password = "pass1",
                        Balance = 200,
                        AccountNumber = "accNo1",
                        IsAdmin = false
                    },
                    new User()
                    {
                        Login = "user2",
                        Password = "pass2",
                        Balance = 500,
                        AccountNumber = "accNo2",
                        IsAdmin = false
                    },
                    new User()
                    {
                        Login = "admin",
                        Password = "admin",
                        Balance = 400,
                        AccountNumber = "accNo3",
                        IsAdmin = true
                    }
                };
            }
        }
    }
}
